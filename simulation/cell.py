# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    cell.py                                            :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tony <tony@student.42.fr>                  +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/26 21:46:10 by tony              #+#    #+#              #
#    Updated: 2020/03/29 06:22:08 by tony             ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

from p5 import *
import random
from simulation.constants import *
import names

class Cell(object):
    def __init__(self, x, y, id=0):
        # Cell information
        self.pos            = Vector(x, y)
        self.name           = names.get_full_name(gender="Male")

        # Probabilities
        self.infection_rate = 0

        # Data
        self.infected_at    = None
        self.infected_by    = None
        self.virus          = None
        self.has_infected   = []
        self.infected       = False

    def move(self, canvas):
        velocity = Vector(random.randint(-1, 1) * MOVE_RATE, random.randint(-1, 1) * MOVE_RATE)
        tmp_pos = velocity + self.pos

        while canvas.out_of_bound(tmp_pos):
            velocity = Vector(random.randint(-1, 1) * MOVE_RATE, random.randint(-1, 1) * MOVE_RATE)
            tmp_pos = velocity + self.pos
            
        self.pos = tmp_pos

    def draw(self):
        if DEBUG:
            no_fill()
            stroke(255)
            ellipse(tuple(self.pos), CELL_COVER, CELL_COVER)
            
        no_stroke()
        fill(255)

        if self.infected:
            fill(red=255, green=0, blue=0)

        ellipse(tuple(self.pos), CELL_SIZE, CELL_SIZE)

    def has_contact(self, cell):
        return self.pos.dist(cell.pos) < CELL_COVER

    def infect(self, cell):
        if self.has_contact(cell):
            if cell.infected is False:
                cell.infected = True
                cell.infected_at = frame_count
                cell.infected_by = self.name
                self.has_infected.append(cell.name)

    def __str__(self):
        print(f"================================== {self.name} ==================================")
        print(f"Infected at : {self.infected_at}")
        print(f"Infected by : {self.infected_by}")
        print(f"Has infected : \n")
        for cell in self.has_infected:
            print(cell)

