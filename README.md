# Virus propagation

## Français 🇫🇷

### Présentation
Cette simulation vise à recréer de manière intuitive la propagation d'un virus, celle-ci ne prétend pas coller à la réalité mais simplement recréer un modèle basique de transmission et de visualisation.


### Modèle

Notre modèle est assez basique, il s'apparente à peu près à celui de la propagation d'un virus et comprend notamment plusieurs étapes dont : 
1. Transmission
2. Temps d'incubation
3. Infectation

### Objets

Pour notre projet, nous définirons trois objets afin de rendre notre simulation plus modulaire : 
1. Virus
2. Cell
3. Grid

## English 🇺🇸

### Introduction
This simulation aims to intuitively recreate the propagation of a virus, this doesn't pretend to be as real as possible but juste to create a basic model of propagation and visualization.

### Model

Our model is pretty basic, it looks like the same as the propagation of a real virus and is especially composed of the following steps : 
1. Transmission
2. Incubation time
3. Infection

### Objects

For our projects, we'll need to define three main objects for modularity purposes :
1. Virus
2. Cell
3. Canvas


