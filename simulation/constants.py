from p5 import *
from enum import Enum
from simulation.virus import Virus

# =============================== Window ===============================
WIN_HEIGHT      = 900
WIN_WIDTH       = 900

# =============================== Canvas ===============================
CANVAS_HEIGHT   = 600
CANVAS_WIDTH    = 600
CANVAS_XOFFSET  = 30
CANVAS_YOFFSET  = 30

# =============================== Cells ===============================
CELL_NUMBERS    = 20
CELL_SIZE       = 10
CELL_RADIUS     = CELL_SIZE // 2
CELL_COVER      = CELL_SIZE + 40

# =============================== Frame ===============================
FRAME_RATE      = 1

# =============================== Physics ===============================
MOVE_RATE       = 10

# =============================== Debug ===============================
DEBUG           = False

# =============================== Style ===============================
class E_Colors(Enum):
    INFECTED = (255, 0, 0)
    RECOVERED = (0, 0, 255)

class E_Virus(Enum):
    CORONAVIRUS = Virus("Covid-19", 4, .02, 10)

